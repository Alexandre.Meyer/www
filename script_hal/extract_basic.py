import requests

def get_publications_from_hal(author_id):
    # URL pour l'API de HAL
    url = f"https://api.archives-ouvertes.fr/search/?q=authIdHal_s:{author_id}&wt=json&fl=title_s,authFullName_s,producedDateY_i"

    # Envoyer une requête GET à l'API de HAL
    response = requests.get(url)

    # Vérifier si la requête a réussi
    if response.status_code == 200:
        data = response.json()
        publications = data['response']['docs']
        return publications
    else:
        print(f"Erreur : Impossible de récupérer les données de l'API de HAL. Code de statut : {response.status_code}")
        return None

# Exemple d'utilisation
author_id = "alexandre-meyer"
publications = get_publications_from_hal(author_id)

if publications:
    for pub in publications:
        print(f"Titre : {pub['title_s']}, Auteurs : {pub['authFullName_s']}, Année : {pub['producedDateY_i']}")