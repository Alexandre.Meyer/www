---
---


<div style="display: flex; align-items: center; padding: 50px;">
  <img src="images/alex.png" alt="Alexandre Meyer" style="margin-right: 100px;">
  <p>
    <strong>Alexandre Meyer</strong><br>
    Maître de Conférences (HDR)<br><br>
    Université Lyon 1 - LIRIS<br>
    Bâtiment Nautibus<br>
    43, Boulevard du 11 Novembre 1918<br>
    69622 Villeurbanne Cedex<br><br>
    Phone: (+33) (0)4 72 44 81 92<br>
    e-mail: alexandre.meyer -at- univ-lyon1.fr
  </p>
</div>


I am Assistant Professor (HDR) in the Computer Science Department at the [Université Lyon 1](https://www.univ-lyon1.fr/) since 2004. I do my research in the [SAARA](https://liris.cnrs.fr/equipe/saara) group of the [LIRIS](https://liris.cnrs.fr) Lab. I received my doctorate in Computer Science from the [Université Grenoble 1](https://www.univ-grenoble-alpes.fr/) in 2001, prepared in the [iMAGIS/EVASION-INRIA](http://www-evasion.imag.fr/) team and 6 months in the [LIGUM Lab (Université de Montréal)](http://www.ligum.umontreal.ca/index_fr.html). From 2002 to 2003, I was a postdoctoral fellow at [University College London](https://www.ucl.ac.uk/) in the [Virtual Environments and Computer Graphics](http://vecg.cs.ucl.ac.uk/) group.

![im_all.png](images/band_main.jpg)

