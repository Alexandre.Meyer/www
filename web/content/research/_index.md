---
title: "Research"
description: "Alexandre Meyer's research"
draft: false
---


## Research interests
My research is at the interface between computer graphics and computer vision, with a large part of machine learning (deep learning). In computer vision, my work focuses on the recognition of expressions from faces and body movements, often making a link with animation or perception. On the computer graphics side, my work ranges from procedural animation (zero data) to learning based animation (deep animation), with a special interest in editing or creating style in animations.

![im_all.png](../images/band_research.jpg)



## Publications

[List of publications](publications)
* [LIRIS](https://liris.cnrs.fr/membres?idn=ameyer&set_language=fr)
* [GoogleScholar](https://scholar.google.com/citations?user=hfS9EakAAAAJ&hl=fr)
* [DBLP](https://dblp.org/pid/37/1744.html)
* [HAL](https://cv.archives-ouvertes.fr/alexandre-meyer)
* [ORCID](https://orcid.org/0000-0002-0249-1048)


Awards
* VISIGRAPP 2024: best [poster](https://liris.cnrs.fr/en/news/best-poster-award-visigrapp-conference) award for the paper ["Pure Physics-Based Hand Interaction in VR"](https://theses.hal.science/ENGEES/hal-04509013v1) (M.B. Mahdi)
* CASA 2021: best paper award for the paper ["Learning-based pose edition for efficient and interactive design"](https://arxiv.org/abs/2107.00397) (L. Victor)
* ICMI 2017: Honorable mention award for the paper ["Toward an efficient body expression recognition based on the synthesis of a neutral movement"](https://hal.science/hal-01675222) (A. Crenn)
 


## Ph-D Thesis supervisions
* Axel Bessy (2024 to ...) with Thomas Barba (HCL), Mathieu Lefort and Hamid Ladjal and ATOS.
* Nathan Salazar (2024 to ...) with Emmanuel Delandrea and Mathieu Lefort.
* Mohammed Bashir (2022 to ...) with Erwan Guillou and Saida Bouakaz.
* Arthur Crenn (2015 to 2019) with Hubert Konik (St-Etienne) and Saida Bouakaz. He is now an engineer at DynamiXYZ (Take Two Interactive).
* Mehdi-Antoine Mahfoudi (2020 to January 2024) with Saida Bouakaz and the SPIROPS company, Thibaut Godin and Axel Buendia.
* Léon Victor (2019 to April 2023) with Saida Bouakaz. He is now at Esker.
* Rizwan Ahmed Khan (2010 to 2013) with Hubert Konik (St-Etienne) and Saida Bouakaz. He is now Assistant Professor in Pakistan.
* Ahmad Abdul Karim (2009 to 2012) with Saida Bouakaz and SPIROPS (Thibaut Godin and Axel Buendia). He works at Rockstar Games in UK.
* Ludovic Dutreve (2007 to 2011) with Saida Bouakaz. He is project manager at Viseo in Grenoble.


### Post-doc and ingeneers
* Remi Casado, 2018
* Rizwan Ahmed Khan, 2017
* Houssam Hnaidi, 2016
* Arthur Crenn, 2020



## Responsabilities

Since September 2022, I am in charge of the SAARA team at LIRIS, with Hamid Ladjal.

Member of program committees
* Since 2023, associated editor of the journal "The Visual Computer"
* Pacific graphics 2023, 2024
* Robovis 2024, 2025
* VISIGRAPP 2024, 2025
* CASA 2023, 2025
* Web3D 2024
* ...

Reviewers for 
* WSCG 2008 to 2025
* Eurographics 2024
* ...

