---
title: "Teaching"
description: "Teaching"
---


I teach algorithm, programming, computer graphics, computer animation and since 2022 machine learning for images.

![im_all.png](../images/band_teaching.jpg)


## 2024/25

### Autumn semester
* L1 : [LIFAPI Introduction Algo/Prog](https://perso.univ-lyon1.fr/elodie.desseree/LIFAPI/) (TD 18h et TP 25h)
* L3 : LIFPROJET Projet Informatique (TP)
* M2 ID3D : [Animation de personnage](http://alexandre.meyer.pages.univ-lyon1.fr/m2-animation/) (CM,TD,TP) du cours ACAMP
* M2 ID3D : [Image et Deep Learning](http://alexandre.meyer.pages.univ-lyon1.fr/m2-apprentissage-profond-image/am/tp_nnfromscratch/)

### Spring semester
* L1 : [LIFAMI Application en Math et en Info](http://licence-info.univ-lyon1.fr/LIFAMI) (CM, TD, TP)
* L2 : [LIFAPCD Conception et Développement d'Applications](http://licence-info.univ-lyon1.fr/LIFAPCD) (CM 4h,TD 12h, TP 40h)
* L3 : LIFPROJET Projet Informatique (TP)
* M1 : [Partie Animation de personnage](http://alexandre.meyer.pages.univ-lyon1.fr/m1if37-animation/) du cours "Animation en Synthèse d'image" (Option du semestre 2, CM,TP)
* L3 : [LIFSTAGE UE Stage de Licence](http://licence-info.univ-lyon1.fr/LIFSTAGE) (with Elodie Desserée)


## Responsabilities

In charge of the Computer Science Bachelor  ("Licence d'informatique") at Lyon 1 University from 2007 to 2020 with E. Desserée, then again since January 2024 with H. Ladjal: 3 accreditations, management of the intership of the third year studends (more than 100 evey year), committees for external admissions (parcourSup, eCandidat), promoting the diploma (Salon étudiant, JES, etc.), ... I was Lead Teacher (Enseignant Référent) from 2010 until end of 2023.

